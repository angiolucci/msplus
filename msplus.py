#!/usr/bin/python
# coding: UTF-8
#
# MSPlus - Ferramenta de monitoramento para sistemas computacionais
#
# Copyright © 2015 Elizeu Elieber Fachini
#
# This file is part of MSPlus.
#
# MSPlus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

rev = '3.0 Rev 030'
from optparse import OptionParser
import sys
import os
import optparse
import rrdtool
import time
import subprocess
import locale
from multiprocessing import Process 
locale.setlocale(locale.LC_ALL, 'C')

class cpu:
	def __init__(self):
		self.file = open('/proc/stat', 'r')
		s = self.file.read()
		dados = s.splitlines()
		result = dados[0].split()
		self.size = len(result)

	def type(self):
		return "DERIVE"

	def close(self):
		self.file.close()

	def export(self):
		clss = cpu()
		self.size = len(clss.start())
		repl = export()
		database = []
		name = clss.name()[0]
		count = 0

		while count<len(name):
			data = repl.fetched(directory, name[count])
			d= []
			d = repl.join(database, data, name[count])
			database = d
			count+=1
		database = repl.resetTime(database)
		repl.write(database, directory, 'CPU')
		return 'The CPU Files was created with successful'

	def start(self):
		self.file.seek(0)
		s = self.file.read()
		dados = s.splitlines()
		result = dados[0].split()
		self.size = len(result)
		return result

	def name(self):
		if(self.size == 9):
			result = [["cpu_user", "cpu_nice", "cpu_system", "cpu_idle", "cpu_iowait", "cpu_irq", "cpu_softirq"]]
		elif(self.size == 10):
			result = [["cpu_user", "cpu_nice", "cpu_system", "cpu_idle", "cpu_iowait", "cpu_irq", "cpu_softirq", "cpu_steal"]]
		elif(self.size == 11):
			result = [["cpu_user", "cpu_nice", "cpu_system", "cpu_idle", "cpu_iowait", "cpu_irq", "cpu_softirq", "cpu_steal", "cpu_guest"]]
		else:
			result = [["cpu_user", "cpu_nice", "cpu_system", "cpu_idle"]]
		return result

	def process(self, result):
		dados = result
		cpu = result[1]


		if(cpu[0]== "cpu"):

			if(len(cpu)== 9):
				result = [cpu[1], cpu[2], cpu[3], cpu[4], cpu[5], cpu[6], cpu[7]]
			elif(len(cpu)== 10):
				result = [cpu[1], cpu[2], cpu[3], cpu[4], cpu[5], cpu[6], cpu[7], cpu[8]]
			elif(len(cpu)== 11):
				result = [cpu[1], cpu[2], cpu[3], cpu[4], cpu[5], cpu[6], cpu[7], cpu[8], cpu[9]]
			else:
				result = [cpu[1], cpu[2], cpu[3], cpu[4]]
			finalResult = [dados[0], [result]]
			return finalResult

	def graph(self):
		metricName = "cpu"
		count = 0
		defline = []
		clss = cpu()
		nameMetrics = []
		while count < 9:
			name = clss.name()[0][count]
			status = os.path.isfile('%s%s.rrd' % (directory, clss.name()[0][count]))
			if status == True:
				nameMetrics.append(name)				
			count+=1

		count = 0		
		graph = graphics()
		s = rrdtool.first('%s%s.rrd' % (directory, clss.name()[0][0]))
		e = rrdtool.last('%s%s.rrd' % (directory, clss.name()[0][0]))

		while count < len(nameMetrics):
			name = clss.name()[0][count]
			defline.append('DEF:'+name+'='+directory+name+'.rrd:Valor:AVERAGE')
			defline.append('LINE1:'+name+'#'+graph.colors[count]+':'+name)
			count+=1
		result = graph.process(metricName, defline, s, e, 1000)

		return result

class graphics:

	def __init__(self):
		self.sampleType = "AVERAGE"
		self.colors = ['000000', 'FF0000', '32CD32', 'FFD700', '0000FF', 'FF00FF', '00FFFF',
		'FF7F24', '050A02', '8B0000', 'ABCDEF', '4682B4', 'E0FFFF', 'CCAADD',
		'005500', 'AA5500', '0055AA', '050A02', '0F000A', 'A000BC', 'D0000D']

	def process(self, name, defline, start, end, base):
		defl = ['%s%s.png'%(directory, name), '--start', '%s' %(start), '--end', '%s' %(end), 
		'--base', '%s'%(base), '--width', '1200', '--height', '590', '--rigid']
		count = 0

		while count < len(defline):
			defl.append(defline[count])
			count+=1
		result = rrdtool.graph(defl)

		return result

class network:
	def __init__(self, interface):
		self.file = open('/proc/net/dev', 'r')
		self.interface = interface

	def type(self):
		return "DERIVE"

	def close(self):
		self.file.close()

	def name(self):
		count = 0
		result = []
		
		while count < len(self.interface):
			result.append([self.interface[count]+"_down", self.interface[count]+"_up"])
		
			count+=1
		return result

	def export(self):
		interface = self.interface
		countDevice = 0

		while countDevice < len(interface):
			clss = network(self.interface)
			repl = export()
			database = []
			name = clss.name()[countDevice]
			count = 0

			while count<len(name):
				data = repl.fetched(directory, name[count])
				d = []
				d = repl.join(database, data, name[count])
				database = d
				count+=1
			database = repl.resetTime(database)
			repl.write(database, directory, "network"+'_'+self.interface[countDevice])
			countDevice+=1
		return "The Network Files was created with successful."

	def start(self):
		self.file.seek(0)
		s = self.file.read()
		dados = s.splitlines()
		count = 0
		spl = ""
		
		while count < len(dados):
			spl = str(dados[count])
			spl = spl.split( )		
			dados[count] = spl
			count+=1
		count = 0
		result = []

		while count < len(dados):
			firstData = dados[count][0].split(':')
			countx = 0
			while countx < len(firstData):
				if(firstData[countx] == ''):
					del(firstData[countx])
				countx+=1
			del(dados[count][0])
			dados[count] = firstData + dados[count]
			count+=1
				

		count = 0	

		while count < len(self.interface):
			counter = 0
									
			while counter < len(dados):
				if dados[counter][0] == self.interface[count]:
					result.append(dados[counter])
				counter+=1
			count+=1
		return result

	def process(self, result):
		dados = result
		count = 0
		result = []

		while count < len(dados[1]):
			counter = 0

			while counter < len(self.interface):
				print(dados[1][count][0])
				if dados[1][count][0] == self.interface[counter]:
					result.append([dados[1][count][1], dados[1][count][9]])
				counter+=1
			count+=1
		result = [dados[0], result]    
		print(result)
		return result

	def graph(self):
		count = 0

		clss = network(self.interface)
		graph = graphics()
		result = []

		while count < len(self.interface):
			device = "network_" + self.interface[count]
			defline = []
			s = rrdtool.first('%s%s.rrd' % (directory, clss.name()[count][0]))
			e = rrdtool.last('%s%s.rrd' % (directory, clss.name()[count][0]))
			counter = 0

			while counter < len(clss.name()[count]):
				name = clss.name()[count][counter]
				defline.append('DEF:'+name+'='+directory+name+'.rrd:Valor:AVERAGE')  
				defline.append('LINE1:'+name+'#'+graph.colors[counter]+':'+name)
				counter+=1
			result.append(graph.process(device, defline, s, e, 1024))
			count+=1

		return result

class memory:
	def __init__(self):
		self.file = open('/proc/meminfo', 'r')

	def export(self):
		c=0
		mem = memory()
		repl = export()
		database = []
		memor = mem.name()[0]

		while c<len(memor):
			data = repl.fetched(directory, memor[c])
			d= []
			d = repl.join(database, data, memor[c])
			database = d
			c+=1
		database = repl.resetTime(database)
		repl.write(database, directory, 'Memory')
		return 'The Memory Files was created successful'

	def type(self):
		return "GAUGE"

	def close(self):
		self.file.close()

	def typeChart(self):
		return ["AREA", "AREA", "AREA", "LINE2", "AREA", "AREA", "AREA", "AREA", "AREA", "LINE2", "LINE2", "LINE2", "LINE2"]

	def name(self):
		result = [["mem_slab",
		"mem_swapc",
		"mem_page",
		"mem_vmalloc",
		"mem_apps",
		"mem_free",
		"mem_buffers",
		"mem_cached",
		"mem_swap",
		"mem_committed",
		"mem_mapped", 
		"mem_active",
		"mem_inactive"]]
		
		return result

	def start(self):
		self.file.seek(0)
		s = self.file.read()
		dados = s.splitlines()

		count = 0

		while count < len(dados):
			spl = str(dados[count])
			spl = spl.split( )
			dados[count] = spl
			count+=1
		count = 0
		number = 0

		while count< len(dados):
			number = float(dados[count][1])
			number = number * 1024
			dados[count][1] = number
			count+=1

		return dados

	def process(self, result):
		count = 0
		dados = result[1]

		while count < len(dados):
			if dados[count][0] == "Active:":
				Active = dados[count][1]
			if dados[count][0] == "Buffers:":
				Buffers = dados[count][1]
			if dados[count][0] == "Cached:":
				Cached = dados[count][1]
			if dados[count][0] == "Committed_AS:":
				Committed_AS = dados[count][1]
			if dados[count][0] == "Inactive:":
				Inactive = dados[count][1]
			if dados[count][0] == "Mapped:":
				Mapped = dados[count][1]
			if dados[count][0] == "MemFree:":
				MemFree = dados[count][1]
			if dados[count][0] == "MemTotal:":
				MemTotal = dados[count][1]
			if dados[count][0] == "PageTables:":
				PageTables = dados[count][1]
			if dados[count][0] == "Slab:":
				Slab = dados[count][1]
			if dados[count][0] == "SwapCached:":
				SwapCached = dados[count][1]
			if dados[count][0] == "SwapCached:":
				SwapCached = dados[count][1]
			if dados[count][0] == "SwapFree:":
				SwapFree = dados[count][1]
			if dados[count][0] == "SwapTotal:":
				SwapTotal = dados[count][1]
			if dados[count][0] == "VmallocUsed:":
				VmallocUsed = dados[count][1]
			count+=1
		App = MemTotal - MemFree - Buffers - Cached - Slab - PageTables - SwapCached
		Swap = SwapTotal - SwapFree 
		result = [result[0], [[Slab, SwapCached, PageTables, VmallocUsed, App, MemFree, Buffers, Cached, Swap, Committed_AS, Mapped, Active, Inactive]]]
		return result

	def graph(self):
		device = "memory"
		count = 0
		graph = graphics()
		defl = []
		defArea = []
		defLine = []
		s = rrdtool.first('%s%s.rrd' % (directory, memory.name(self)[0][0]))
		e = rrdtool.last('%s%s.rrd' % (directory, memory.name(self)[0][0]))

		while count < len(memory.name(self)[0]):
			name = memory.name(self)[0][count]
			tChart = memory.typeChart(self)[count]  
			defl.append('DEF:'+name+'='+directory+name+'.rrd:Valor:AVERAGE')
			defl.append('CDEF:'+name+'_mod='+name)
			if tChart == 'AREA':
				defArea.append(tChart+':'+name+'_mod#'+graph.colors[count]+':'+name+':STACK')
			else:
				defLine.append(tChart+':'+name+'_mod#'+graph.colors[count]+':'+name)
			count+=1
		defl += defArea + defLine
		result = graph.process(device, defl, s, e, 1024)
		return result

class export:
	def __init__(self):
		self.line =[]

	def fetched(self, directory, name):
		first = rrdtool.first(directory+name+".rrd")
		last = rrdtool.last(directory+name+".rrd")
		text = ['rrdtool', 'fetch', directory+name+'.rrd', 'AVERAGE', '--start', str(first),  '--end', str(last)]
		fetch = subprocess.check_output(text)
		line = fetch.splitlines()
		countex=2
		spl = []

		while countex<len(line):
			replace = line[countex].replace(':',' ')
			spl.append(replace.split())
			countex+=1
		return spl

	def join(self, principal, secondary, secondaryName):

		if principal == []:
			principal = secondary
			principal.insert(0, ['time', secondaryName])
		else:
			principal[0].append(secondaryName)
			counter = 1

			while counter < len(principal):
				cont = counter-1

				if principal[counter][0]==secondary[cont][0]:
					principal[counter].append(secondary[counter-1][1])
				counter+=1
		return principal

	def resetTime(self, data):
		timeFirst = int(data[1][0])
		n = 1

		while n < len(data):
			timeN = int(data[n][0])
			data[n][0] = str(timeN - timeFirst)
			n+=1
		return(data)

	def write(self, result, directory, name):
		fileOpen = open("%s%s.txt"%(directory, name), "w")
		count = 0

		while count < len(result):
			countValue = 0

			while countValue < len(result[count]):
				fileOpen.write(result[count][countValue] + " ")
				countValue+=1    
			fileOpen.write("\n")
			count+=1
		fileOpen.close()

class storage:

	def __init__(self, storage):
		self.storage = storage
		self.prev_time = 0
		self.prev_result = []
		files = []
		count = 0
		self.bytes_per_sector = []

		while count < len(self.storage):
			try:
				bytes_sec = open('/sys/block/%s/queue/logical_block_size'%(storage[count]), 'r')
				self.bytes_per_sector.append(bytes_sec.read())
			except:
				print("-")

			count+=1

		try:
			count = 0

			while count < len(storage):
				files.append(open('/sys/block/%s/stat'%(self.storage[count]), 'r'))
				count+=1
			self.file = files
			self.storageAddress = "stat"
		except:
			files.append(open('/proc/diskstats'))
			self.file = files
			self.storageAddress = "diskstat"

	def type(self):
		return "GAUGE"

	def close(self):
		self.file.close()

	def start(self):
		result = []
		if self.storageAddress == "stat":
			count = 0

			while count < len(self.file):
				self.file[count].seek(0)
				dados = ['', '', self.storage[count]]
				dados.extend(self.file[count].read().split())
				result.append(dados)
				count += 1

		if self.storageAddress == "diskstat":
			self.file[0].seek(0)
			split = self.file[0].read().splitlines()
			count = 0

			while count < len(split):
				line = []
				line = split[count].split()
				split[count] = line
				count+=1
			count = 0
            
			while count < len(self.storage):
				counter = 0

				while counter < len(split):
					if self.storage[count]==split[counter][2]:
						result.append(split[counter])
					counter+=1
				count+=1
		count = 0
		return result

	def name(self):
		resultName = []
		count = 0

		while count < len(self.storage):
			resultName.append([self.storage[count]+"_svctm",
				self.storage[count]+"_avgwait",
				self.storage[count]+"_avgrdwait",
				self.storage[count]+"_avgwrwait",
				self.storage[count]+"_util",
				self.storage[count]+"_rdbytes",
				self.storage[count]+"_wrbytes",
				self.storage[count]+"_rdio",
				self.storage[count]+"_wrio",
				self.storage[count]+"_avgrdrqsz", 
				self.storage[count]+"_avgwrrqsz"])
			count+=1    
		return resultName

	def process(self, result):
		count = 0
		finalResult = []

		if self.prev_result == []:
			self.prev_result = result[1]
			self.prev_time = result[0]
		else:
			interval    = result[0]- self.prev_time
			if(interval < 1):
				interval = 1
			self.prev_time = result[0]

			while count < len(self.storage):
				read_ios    = int(result[1][count][3]) - int(self.prev_result[count][3])
				write_ios   = int(result[1][count][7]) - int(self.prev_result[count][7])
				rd_ticks    = int(result[1][count][6]) - int(self.prev_result[count][6])
				wr_ticks    = int(result[1][count][10])- int(self.prev_result[count][10])
				rd_sectors  = int(result[1][count][5]) - int(self.prev_result[count][5])
				wr_sectors  = int(result[1][count][9]) - int(self.prev_result[count][9])
				tot_ticks   = int(result[1][count][12])- int(self.prev_result[count][12])
				read_io_per_sec = read_ios/interval
				write_io_per_sec = write_ios/interval
				read_bytes_per_sec = rd_sectors/interval * int(self.bytes_per_sector[count])
				write_bytes_per_sec = wr_sectors/interval* int(self.bytes_per_sector[count])
				total_ios = read_ios + write_ios
				total_ios_per_sec = total_ios/interval
				utilization = tot_ticks / interval

				if total_ios_per_sec!= 0:
					servicetime_in_sec = utilization / total_ios_per_sec / 1000
				else:
					servicetime_in_sec = 0

				if 0 != total_ios:
					average_wait_in_sec = (rd_ticks + wr_ticks) / total_ios / 1000
					average_rd_wait_in_sec = rd_ticks / total_ios / 1000
					average_wr_wait_in_sec = wr_ticks / total_ios / 1000
				else:
					average_wait_in_sec = 0
					average_rd_wait_in_sec = 0
					average_wr_wait_in_sec = 0

				if average_rd_wait_in_sec < 0:
					average_rd_wait_in_sec = 0

				if average_wr_wait_in_sec < 0:
					average_wr_wait_in_sec = 0

				if read_ios != 0:
					average_rd_rq_size_in_kb = rd_sectors * int(self.bytes_per_sector[count]) / 1000/ read_ios
				else:
					average_rd_rq_size_in_kb = 0

				if write_ios != 0:
					average_wr_rq_size_in_kb = wr_sectors * int(self.bytes_per_sector[count]) / 1000/ write_ios
				else:
					average_wr_rq_size_in_kb = 0

				util_print = utilization / 10
				svctm       = servicetime_in_sec
				avgwait     = average_wait_in_sec
				avgrdwait   = average_rd_wait_in_sec
				avgwrwait   = average_wr_wait_in_sec
				util        = util_print
				rdbytes     = read_bytes_per_sec
				wrbytes     = write_bytes_per_sec
				rdio        = read_io_per_sec
				wrio        = write_io_per_sec
				avgrdrqsz   = average_rd_rq_size_in_kb
				avgwrrqsz   = average_wr_rq_size_in_kb
				finalResult.append([svctm, avgwait, avgrdwait, avgwrwait, util, rdbytes, wrbytes, rdio, wrio, avgrdrqsz, avgwrrqsz])
				count+=1
		fResult = [result[0], finalResult]
		self.prev_result = result[1]
		return fResult        

	def export(self):
		countDevice = 0

		while countDevice < len(self.storage):
			clss = storage(self.storage)
			repl = export()
			name = clss.name()[countDevice]
			nameGraph = [["latency", 4], ["utilization", 1], ["throughput", 2], ["iops", 2], ["avg_iops", 2]]
			countGraph = 0
			countMetric = 0

			while countGraph < len(nameGraph):
				count = 0
				database = []

				while count<nameGraph[countGraph][1]:
					data = repl.fetched(directory, name[countMetric])
					d = []
					d = repl.join(database, data, name[countMetric])
					database = d
					countMetric+=1
					count+=1
				database = repl.resetTime(database)
				repl.write(database, directory, 'storage_'+self.storage[countDevice]+'_%s'%(nameGraph[countGraph][0]))
				countGraph+=1
			countDevice+=1
		return "The Storage files was created with successful"

	def graph(self):
		count = 0
		defline = []
		clss = storage(self.storage)
		graph = graphics()
		result = []

		while count < len(clss.name()):
			x = 0
			s = rrdtool.first('%s%s.rrd' % (directory, clss.name()[count][0]))
			e = rrdtool.last('%s%s.rrd' % (directory, clss.name()[count][0]))
			nameGraph = [["latency", 4], ["utilization", 1], ["throughput", 2], ["iops", 2], ["avg_iops", 2]]
			counter = 0
			counterExt = 0

			while x < len(nameGraph):
				defline = []
				metricName = "storage_" + self.storage[count]+"_"+nameGraph[x][0]
				counterExt += nameGraph[x][1]

				while counter < counterExt:
					name = clss.name()[count][counter]
					defline+=['DEF:'+name+'='+directory+name+'.rrd:Valor:AVERAGE']
					defline+=['LINE1:'+name+'#'+graph.colors[counter]+':'+name]
					counter+=1
				result.append(graph.process(metricName, defline, s, e, 1024))

				x+=1

			count+=1
		return result

def main():
	usage = ("Usage: %prog [OPTION]\n" + "Try --help for more help.")
	description = ("This option captures the samples about device status during the time determined")
	version ="%s" % (rev)
	parser = OptionParser(usage=usage, description=description, version=version)
	parser.add_option("-r", "--run", help="Run the sample capture", dest="run", action="store_true")
	parser.add_option("-g", "--graph", help="Created the graphics", dest="graph", action="store_true")
	parser.add_option("-e", "--export", help="Export the monitoring data to text file", dest="export", action="store_true")
	run_opts = optparse.OptionGroup(parser, 'Run Options')
	run_opts.add_option("-s", "--sample", type="int", default=100, metavar="TIME",
		help="Sets the samples amount that will be captured." + " Default: %default")
	run_opts.add_option("-i", "--interval", type="int", default=1, metavar="TIME", dest="interval",
		help="Sets the Interval that of samples will be captured=." + " Default: %default")
	run_opts.add_option("-b", "--heartBeat", type="int", default=5, metavar="TIME",
		help="heartbeat defines the maximum acceptable interval between samples/updates." + " Default: %default")
	parser.add_option_group(run_opts)
	graph_opts = optparse.OptionGroup(parser, 'Graph Options')
	parser.add_option_group(graph_opts)    

	parser.add_option("-v", "--save", default=0, type="int", help="Save the datas each samples")
	parser.add_option("-m", "--memory", help="Includes the memory on monitoring", action="store_true")
	parser.add_option("-c", "--cpu", help="Includes the CPU on monitoring", action="store_true")
	parser.add_option("-t", "--storage", default=[], help="Includes the storage devices on monitoring", action="append")
	parser.add_option("-n", "--network", default=[], help="Includes the network interfaces on monitoring", action="append")

	(options, args) = parser.parse_args()

	return (options, args)

##Função que  faz o monitoramento 
def monitoring(devices):
	clss = []
	dataBase = []

#Criação de uma lista com os dispositivos junto a campos referentes aos dispositivos na lista de dados.
	count = 0
	while count < len(devices):
		clss.append(devices[count])

		dataBase.append([])

		count+=1
	name = []
	types =[]

	#captura da lista de nomes e tipo de dados dos dispositivos.
	count = 0
	while count < len(devices):
		name.append(clss[count].name())
		types.append(clss[count].type())
		count+=1
	
    #Os arquivos RRD de cada dispositivo são criados.
	print("Creating the RRD files")
	count = 0
	while count < len(name):
		counter = 0
		while counter < len(name[count]):
			counterMetric = 0

			while counterMetric < len(name[count][counter]):
				value = name[count][counter][counterMetric]
				typeValue= types[count]
				rrdtool.create('%s%s.rrd' % (directory, value), '--start', '1401210999', '--step','%d' % (interval),
					'DS:Valor:%s:%d:U:U' % (typeValue, heartBeat), 'RRA:AVERAGE:0.5:1:%d' % (samples))
				counterMetric+=1
			counter+=1
		count+=1
	count = 0
	clss = []

	print('Monitoring the system.')
	while count < len(devices):
		clss.append(devices[count])
		dataBase.append([])
		count+=1
	
	timeNow = time.time()
	nextTime = timeNow
	finalTime = timeNow + (samples * interval) + 1
	
	counter = 0
	countProgress = 0
	timeSave = 0
	saves = save
	while timeNow < finalTime:
		if save + 1 == int(finalTime - timeNow):
			saves = saves + 1
		
		## Define o tempo da próxima amostra, adicionando o intervalo da ultima amostra.
		nextTime = nextTime + interval
		## Captura o tempo atual.        
		timeNow = time.time()
		## Verifica o tempo que necessita esperar para a próxima captura.
		sleep = nextTime - timeNow
		## Tempo de espera do MSPlus para a próxima captura.
		if sleep > 0:
			time.sleep(sleep)
			count = 0
			## Loop de captura dos dados de estado dos dispositivos
		while count < len(devices):
			dado = []
			dado = ([int(timeNow), clss[count].start()])
			dataBase[count].append(dado)
			
			count+=1
			## Contador para salvar os dados a cada um tempo
		## Verifica se o tempo já chegou
		if (timeSave == saves):
			newP = Process(target=DataSaving, args=(dataBase, devices))
			newP.start()
			dataBase = []
			countZ = 0
			while countZ < len(devices):
				dataBase.append([])
				countZ+=1
			timeSave = 0
		timeSave += 1


		##Progresso do monitoramento
		valor = samples + 1
		progress = 100.0/valor * countProgress
		sys.stdout.write("\r%d%%" % progress)
		sys.stdout.flush()
		countProgress +=1.0

	if dataBase != 0:
		newP.join()
		newP = Process(target=DataSaving, args=(dataBase, devices))
		newP.start()   

def DataSaving(dataBase, devices):
	count = 0
	clss = []
	while count < len(devices):
		clss.append(devices[count])
		count+=1

	print('Processing')
	count = 0
	while count < len(dataBase):
		counter = 0

		while counter<len(dataBase[count]):
			data = dataBase[count][counter]
			data = clss[count].process(data)
			dataBase[count][counter] = data
			counter+=1
		count+=1

	count = 0
	name = []
	types =[]
	while count < len(devices):
		name.append(clss[count].name())
		types.append(clss[count].type())
		count+=1

	print("Saving data")    
	countAmostra = 0
	files=""

	while countAmostra < len(dataBase):
		countTime = 0

		while countTime < len(dataBase[countAmostra]):
			timeData = dataBase[countAmostra][countTime][0]
			countQtt = 0

			while countQtt < len(dataBase[countAmostra][countTime][1]):
				countData = 0

				while countData < len(dataBase[countAmostra][countTime][1][countQtt]):
					dataRRD = "%d:%d" % (timeData, float(dataBase[countAmostra][countTime][1][countQtt][countData]))
					files = '%s%s.rrd' % (directory, name[countAmostra][countQtt][countData])

					try:
						rrdtool.update(files, dataRRD)
					except:
						print("Problem with the value of", files, dataRRD)
					countData+=1
				countQtt+=1
			countTime+=1
		countAmostra+=1

countDevice = 0
##Inicia o método Main
if __name__ == '__main__':
	paramet = main()
	devices = []

##Verifica se CPU será usado nas funções
if paramet[0].cpu == True:
	devices.append(cpu())
	countDevice+=1

##Verifica se Memória será usada nas funções
if paramet[0].memory == True:
	devices.append(memory())
	countDevice+=1

##Verifica se o dispositivo de armazenamento será usado nas funções
if paramet[0].storage != []:
	fileStor = open('/proc/diskstats', 'r')
	dadoStor = fileStor.read()
	dadosS = dadoStor.splitlines()

	stor = paramet[0].storage[0]
	devices.append(storage(stor.split(',')))
	storSplit = stor.split(',')
	count = 0
	countStorage = 0
	while count < len(dadosS):
		counter = 0
		dadoS = dadosS[count].split( )
		while counter < len(storSplit):
			if(dadoS[2]==storSplit[counter]):
				countStorage+=1
			counter+=1
		count+=1
	if(countStorage==0):
		sys.exit("Check! It's necessary insert some storage device name.")
	else:
		countDevice+=1

##Verifica se a Rede será usada nas funções
if paramet[0].network != []:
	fileNet = open('/proc/net/dev', 'r')
	dadoNet = fileNet.read()
	dadosN = dadoNet.splitlines()
	netw = paramet[0].network[0]
	netwSplit = netw.split(',')
	devices.append(network(netw.split(',')))
	count = 0
	countInterface = 0
	while count < len(dadosN):
		counter = 0
		dadoN = dadosN[count].split()
		dadoN = dadoN[0].split(':')

		while counter < len(netwSplit):
			if(dadoN[0] == (netwSplit[counter])):
				countInterface += 1
			counter+=1
		count+=1

	if(countInterface==0):
		sys.exit("Check! It's necessary insert some Network interface name.")
	else:
		countDevice+=1
##Analisa se algum dispositivo foi chamado, caso não exibe mensagem e para.        
if(countDevice==0):
	sys.exit("It's necessary insert some device parameter")

## Verifica se foi chamado a função de monitoramento
if paramet[0].run == True:

##Verifica se outros parâmetros opcionais foram chamados
	try:
		samples = int(paramet[0].sample)
	except:
		paramet.error("The monitoring will have 100 samples.")
	try:
		save = int(paramet[0].save)
		saves = 0
		if save == 0:
			save = samples 
			saves = save

	except:
		paramet.error("All data were be saves on monitoring end")

	try:
		interval = int(paramet[0].interval)
	except:
		paramet.error("The capture interval samples will be of 1 second.")

	try:    
		heartBeat = int(paramet[0].heartBeat)
	except:
		paramet.error("The HeartBeat during the monitoring will be 5 seconds")

	try:
		directory = paramet[1][0]
	except:
		sys.exit("It's necessary insert a directory.")
## Inicia o monitoramento.
	monitoring(devices)

	print("Final monitoring")

# Abaixo chama a função de gráfico para cada dispositivo
if paramet[0].graph == True:
	directory = paramet[1][0]
	count=0

	while count < len(devices):
		n = devices[count]
		print(n.graph())
		count+=1

# Abaixo chama a função de exportar os dados para cada dispositivo
if paramet[0].export == True:
	directory = paramet[1][0]    
	count=0

	while count < len(devices):
		n = devices[count]
		print(n.export())
		count+=1
