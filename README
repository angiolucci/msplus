# MSPlus - Ferramenta de monitoramento para Sistemas Computacionais #

MSPlus é um sistema de monitoramento de baixa intrusividade, projetado
para monitorar sistemas computacionais durante um tempo definido. O
MSPlus além de captar os dados de estado dos dispositivos dos sistemas
computacionais, realiza o processamento dos dados e os armazena
através do RRDtool. Com os arquivos armazenados, o MSPlus possibilita
ao usuário gerar gráficos e exportar os dados em arquivo de texto,
para eventuais processamentos externos.

A página do projeto é:** https://bitbucket.org/elizeuelieber/msplus**

MSPlus é um software livre, licenciado nos termos da GNU GPL (veja o
arquivo "COPYING", ou visite o site http://www.gnu.org/licenses/).

Este arquivo descreve como construir e instalar o MSPlus.  


## Dependências ##

A única dependência externa do MSPlus é a biblioteca RRDtool usada
para armazenar os dados no disco. Para instalar em versões mais
recentes dos sistemas operacionais, basta fazer o seguinte passo:

No Ubuntu ou Debian:
   
```
 $ sudo apt-get install python-rrdtool
```
   
No CentOS, Scientific Linux ou Fedora:
   
```
 $ sudo yum install rrdtool-python
```


## Instalação ##

Não é necessário instalar o MSPlus por ser um único script Python,
basta ter uma versão de Python >2.7.

Para ter a aplicação basta utilizar:

Download Direto: 

 [https://bitbucket.org/elizeuelieber/msplus/downloads](Link URL)

ou

Git:

   
```
 $ git clone https://bitbucket.org/elizeuelieber/msplus.git 

```


**Executando**

Para executar o MSPlus no terminal Linux, é necessário que seja feito
a passagem de parâmetros e um argumento independente da função
utilizada. O argumento em questão é o caminho do diretório onde serão
armazenados os arquivos gerados pelas funções do MSPlus. No caso da
função de monitoramento é importante que o usuário crie uma nova
pasta, pois como os arquivos gerados são sempre os mesmos para cada
dispositivo, os novos arquivos irão sobrescrever os arquivos
existentes no diretório.

No caso dos parâmetros, estes representam os modos de operação
(monitoramento, gráfico e exportação), dados opcionais de
monitoramento e os dispositivos, conforme descrito abaixo:

    
```

-r ou --run: Realiza a execução do monitoramento.
        -s <x> ou --sample <x>: Quantidade x de amostras que serão
         captadas durante o monitoramento. 
        -i <x> ou --interval <x>: Intervalo x, em segundos, entre cada
         amostra de monitoramento.
        -b <x> ou --heartbeat <x>: Intervalo máximo x, em segundos,
         aceitável entre amostras. Esse intervalo busca amenizar os
         impactos de eventuais falhas de amostragem. Caso ocorra uma
         falha e o tempo desde a última amostra correta for inferior
         ao do heartbeat, uma taxa média é calculada e aplicada às
         amostras com falhas dentro do intervalo. Se o tempo desde a
         última amostra correta for superior, as amostras com falha
         ficarão com valor "NaN".
    -g ou --graph: Gera os gráficos.  Essa função lê os dados
       armazenados no diretório indicado e cria os gráficos neste
       mesmo diretório.
    -e ou --export: Exporta os dados armazenados no diretório indicado
       para arquivos texto neste mesmo diretório.  
    -c ou --cpu: Inclui as métricas de CPU.
    -m ou --memory: Inclui as métricas de memória.
    -n <if1>,<if2>,... ou --network <if1>,<if2>,...: Inclui as
       métricas das interface de rede listadas.
    -t <sda1>,<sda2>,... ou --storage <sda1>,<sda2>,...: Inclui as
       métricas dos dispositivo de armazenamento listados.
```


Um exemplo de linha de comando no terminal poderia ser:

   
```
 $ ./msplus.py -r -s 300 -i 1 -b -5 -m -c -t sda,sdb /diretorio/
```

Nessa linha do terminal é chamado o MSPlus para executar a função
monitoramento (-r), com 300 amostras (-s) a um intervalo (-i) de 1
segundo e heartbeat (-b) de 5 segundos. Os dispositivos monitorados
serão: memória (-m), cpu (-c) e os discos de armazenamento (-t) sda e
sdb. Todos os arquivos gerados serão armazenados no diretório
"/diretorio/".
